# Font Awesome 6 Alphas

Not my work. These are components of the current Font Awesome 6 Alphas released under open source licenses. Despite commit names, this repository only contains Font Awesome Free.

Font Awesome Free for 6.0.0-beta1 was released on [PyPI](https://pypi.org/project/fontawesomefree/6.0.0b1/#files) by Fort Awesome.
