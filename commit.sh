#!/bin/bash
# Copyright 2021 Eli Bradley, MIT License
 
shopt -s extglob dotglob

md5 *.zip *.whl > md5.txt

if [ "$1" == init ];
then
    rm -rf .git free
    git init
    git remote add origin https://gitlab.com/EtherTyper/font-awesome-6-alpha.git

    git add .
    git commit -m "Initial commit"
elif [[ -f "$1" ]]
then
    ARCHIVE=$1
    NAME=${ARCHIVE%.*}
    EXTENSION=${ARCHIVE##*.}

    if [ $EXTENSION == zip ];
    then
        rm -rf free $NAME
        unzip $ARCHIVE
        cp -r $NAME/fontawesome6/free .
        git add .
        git commit -m $NAME
    elif [ $EXTENSION == whl ];
    then
        rm -rf free fontawesomefree
        unzip $ARCHIVE
        cp -r fontawesomefree/static/fontawesomefree/. free
        git add .
        git commit -m $NAME
    fi
fi
